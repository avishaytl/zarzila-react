import React, { useEffect, useState } from 'react';
 import { Editor } from '@tinymce/tinymce-react';

 function RichEditor(props: any){
    // const [isChange,setChange] = useState(null)
    // useEffect(()=>{
    //   if(isChange)
    //     setTimeout(() => {
    //       if(props.setValue)
    //         props.setValue(isChange)
    //         setChange(null)
    //     }, 2000);
    // },[isChange])
    // function saveChange(){
          
    // }
    function handleEditorChange(content: string) {
        // setChange(content)
        //  console.log('Content was updated:', content);
         if(props.setValue)
            props.setValue(content)
    }
    return (
      <Editor
       apiKey='p4nx9atbm32zzosiwcuf8w6uewznfqy8he0nejljd2ezcbjc'
        initialValue={`<p>${props.value}</p>`}
        init={{
          height: 200,
          menubar: false,
          plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
          ],
          toolbar:
            `undo redo | formatselect | bold italic backcolor | \
            alignleft aligncenter alignright alignjustify | \
            bullist numlist outdent indent | removeformat | help`
        }} 
        onEditorChange={handleEditorChange}
      />
    );
 }
//  class RichEditor extends React.Component {
//     constructor(props: any){
//     super(props)
//     }
//     handleEditorChange = (content: any, editor: any) => {
//         console.log('Content was updated:', content);
//     }

//    render() {
//      return (
//        <Editor
//         apiKey='p4nx9atbm32zzosiwcuf8w6uewznfqy8he0nejljd2ezcbjc'
//          initialValue="<p>This is the initial content of the editor</p>"
//          init={{
//            height: 500,
//            menubar: false,
//            plugins: [
//              'advlist autolink lists link image charmap print preview anchor',
//              'searchreplace visualblocks code fullscreen',
//              'insertdatetime media table paste code help wordcount'
//            ],
//            toolbar:
//              `undo redo | formatselect | bold italic backcolor | \
//              alignleft aligncenter alignright alignjustify | \
//              bullist numlist outdent indent | removeformat | help`
//          }}
//          onEditorChange={this.handleEditorChange}
//        />
//      );
//    }
//  }

 export default RichEditor;