class appServices {
    getCurrentDate = () =>{ 
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var min = String(today.getMinutes()).padStart(2, '0');
        var hour = String(today.getHours()).padStart(2, '0'); //January is 0!
        var sec = String(today.getSeconds()).padStart(2, '0'); //January is 0!
        var yy = today.getFullYear(); 

        return dd + '/' + mm + ' ' + hour + ':' + min + ':' + sec; 
    }
}
const appService = new appServices();
export { appService }