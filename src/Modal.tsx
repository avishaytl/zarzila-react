import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import { Button, Icon, Input } from 'semantic-ui-react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css'; 
import { Dropdown } from 'semantic-ui-react'
import Editor from './Editor';
import Checkbox from '@material-ui/core/Checkbox';
import { useStore } from './stores/rootStore';
import { appService } from './services/appServices';

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    borderRadius          : 0,
    transform             : 'translate(-50%, -50%)'
  },
  overlay: {
      backgroundColor:'rgba(0,0,0,0.8)'
  }
};
 
// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('#root');

function Articles(props: any){
    const [mainTitle,setMainTitle] = useState(props.mainStories[0].item.mainTitle);
    const [subTitle,setSubTitle] = useState(props.mainStories[0].item.subTitle);
    const [mainText,setMainText] = useState(props.mainStories[0].item.mainText);
    const [subText,setSubText] = useState(props.mainStories[0].item.subText); 
    const [img,setImg] = useState(props.mainStories[0].item.img);
    const [createdTime,setCreatedTime] = useState(props.mainStories[0].item.createdAt);
    const [readTime,setReadTime] = useState(props.mainStories[0].item.readTime); 
    const [category,setCategory] = useState(props.mainStories[0].item.category); 
    const [storieIndex,setStorieIndex] = useState(props.mainStories[0].item.index); 
    const [title,setTitle] = useState(props.mainStories[0].item.title); 

    function changeStorie(name: string){
        if(name)
        for(let i = 0;i < props.mainStories.length;i++){
            console.debug('changeStorie',props.mainStories[i].key , name) 
            if(props.mainStories[i].key === name){
                console.debug('changeStorie',props.mainStories[i].key , name) 
                setMainTitle(props.mainStories[i].item.mainTitle)
                setSubTitle(props.mainStories[i].item.subTitle)
                setMainText(props.mainStories[i].item.mainText)
                setSubText(props.mainStories[i].item.subText)
                setCreatedTime(props.mainStories[i].item.createdAt)
                setReadTime(props.mainStories[i].item.readTime) 
                setCategory(props.mainStories[i].item.category)
                setStorieIndex(props.mainStories[i].item.index)
                setTitle(props.mainStories[i].item.title)
                setImg(props.mainStories[i].item.img)
                break;
            }
        }
    } 
    // useEffect(()=>{
    //     return(()=>{
    //         // let index = props.selectedStorie.split('_index_')[0];
    //         // console.debug('index' ,index) 
    //         // setMainTitle(props.mainStories[index].item.mainTitle)
    //         // setSubTitle(props.mainStories[index].item.subTitle)
    //         // setMainText(props.mainStories[index].item.mainText)
    //         // setSubText(props.mainStories[index].item.subText) 
    //     })
    // })
    function changeMainTitle(data: string){
        let index = props.selectedStorie.split('_index_')[0];
        console.debug('index' ,data)   
        props.mainStories[index].item.mainTitle = data;
    }

    function changeSubTitle(data: string){
        let index = props.selectedStorie.split('_index_')[0];
        console.debug('index' ,data) 
        props.mainStories[index].item.subTitle = data; 
    }

    function changeMainText(data: string){
        let index = props.selectedStorie.split('_index_')[0];
        console.debug('index' ,data) 
        props.mainStories[index].item.mainText = data; 
    }

    function changeSubText(data: string){
        let index = props.selectedStorie.split('_index_')[0];
        console.debug('index' ,data) 
        props.mainStories[index].item.subText = data; 
    }

    return(
        <div className={`tabs-container`}>
            <div className={`articles-list-bar`}> 
                <div style={{flex:1}}>
                    <Dropdown
                        className={`footer-icon-form`}
                        placeholder={props.mainStories[0].key}
                        fluid
                        search
                        selection
                        defaultValue={props.mainStories[0].key}
                        options={props.mainStories} 
                        onChange={(e: any,opt)=>{ 
                            let index = props.selectedStorie.split('_index_')[0];
                            console.debug('index' ,index) 
                            setMainTitle(props.mainStories[index].item.mainTitle)
                            setSubTitle(props.mainStories[index].item.subTitle)
                            setMainText(props.mainStories[index].item.mainText)
                            setSubText(props.mainStories[index].item.subText)
                            setTimeout(() => { 
                                props.setSelectedStorie(opt.value as string)
                                changeStorie(e.target.innerText) 
                            }, 0);
                        }} 
                        icon={<Icon style={{opacity:0.5,marginRight:10}} name='list'/>}
                    />  
                </div>
                <div onClick={()=>{
                    props.addStorie()  
                    alert('New Storie Created!')
                }} className={`add-article`}>
                    <Icon name='plus' /> 
                </div>
            </div>
            <div className={`articles-view`}>
                <div className={`articles-bar`}> 
                    <div style={{display:'flex'}}>  
                            <Input className={`footer-icon-form grid-footer-item`}>
                                <label style={{cursor:'pointer'}} className={`input-file`} htmlFor="file-upload">
                                    <Icon name='image' />
                                    <p>Upload</p>
                                </label> 
                                <input onChange={(e)=>{
                                    console.debug('e',e.target.value)
                                    setImg(e.target.value)
                                    let index = props.selectedStorie.split('_index_')[0];
                                    console.debug('index' ,index) 
                                    props.mainStories[index].item.img = e.target.value; 
                                }} id={'file-upload'} type={'file'}/> 
                            </Input>   
                        <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Url'>
                            <Icon name='image' />
                            <input onChange={(e)=>{ 
                                setImg(e.target.value)
                                let index = props.selectedStorie.split('_index_')[0];
                                console.debug('index' ,index) 
                                props.mainStories[index].item.img = e.target.value; 
                            }} value={img} />
                        </Input> 
                        <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Title'>
                            <Icon name='heading' />
                            <input onChange={(e)=>{ 
                                setTitle(e.target.value)
                                let index = props.selectedStorie.split('_index_')[0];
                                console.debug('index' ,index) 
                                props.mainStories[index].item.title = e.target.value; 
                            }} value={title} />
                        </Input> 
                    </div>
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Position'>
                        <Icon name='sort numeric down' />
                        <input onChange={(e)=>{ 
                            setStorieIndex(e.target.value)
                            let index = props.selectedStorie.split('_index_')[0];
                            console.debug('index' ,index) 
                            props.mainStories[index].item.index = e.target.value; 
                        }} value={storieIndex} />
                    </Input> 
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Read Time'>
                        <Icon name='star outline' />
                        <input onChange={(e)=>{ 
                            setCategory(e.target.value)
                            let index = props.selectedStorie.split('_index_')[0];
                            console.debug('index' ,index) 
                            props.mainStories[index].item.category = e.target.value; 
                        }} value={category} />
                    </Input> 
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Created At'>
                        <Icon name='calendar' />
                        <input onChange={(e)=>{ 
                            setCreatedTime(e.target.value)
                            let index = props.selectedStorie.split('_index_')[0];
                            console.debug('index' ,index) 
                            props.mainStories[index].item.createdAt = e.target.value; 
                        }} value={createdTime} />
                    </Input>
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Read Time'>
                        <Icon name='time' />
                        <input onChange={(e)=>{ 
                            setReadTime(e.target.value)
                            let index = props.selectedStorie.split('_index_')[0];
                            console.debug('index' ,index) 
                            props.mainStories[index].item.readTime = e.target.value; 
                        }} value={readTime} />
                    </Input> 
                </div>
                <div className={`articles-view-form`}>
                    <div className={`articles-view-item`}><p>Main Title</p> 
                        <Editor value={mainTitle} setValue={changeMainTitle} type={'main-title'}/> 
                    </div>
                    <div className={`articles-view-item`}><p>Sub Title</p>
                        <Editor value={subTitle} setValue={changeSubTitle} type={'sub-title'}/> 
                    </div>
                    <div className={`articles-view-item`}><p>Main Text</p>
                        <Editor value={mainText} setValue={changeMainText} type={'main-text'}/> 
                    </div>  
                    <div className={`articles-view-item`}><p>Sub Text</p>
                        <Editor value={subText} setValue={changeSubText} type={'sub-text'}/> 
                    </div>
                </div>
                <div className={`footer-articles-view`}>
                    <Button onClick={()=>{ 
                        let index = props.selectedStorie.split('_index_')[0];
                        console.debug('index' ,index) 
                        setMainTitle(props.mainStories[index].item.mainTitle)
                        setSubTitle(props.mainStories[index].item.subTitle)
                        setMainText(props.mainStories[index].item.mainText)
                        setSubText(props.mainStories[index].item.subText)
                        props.saveStorie() 
                    }} className={`footer-btn`}> 
                        Save Changes
                    </Button> 
                    <Button onClick={()=>{ 
                        props.deleteStorie() 
                        props.closeModal() 
                    }} className={`footer-btn`}> 
                        Delete Storie
                    </Button> 
                    <Button onClick={()=>{ 
                        props.closeModal() 
                    }} className={`footer-btn`}> 
                        Cancel
                    </Button> 
                </div>
            </div>
        </div>
    )
}
function Advertisements(props: any){
    const [isActive,setIsActive] = useState(props.ad[0].isActive);
    const [url,setUrl] = useState(props.ad[0].url);
    const [src,setSrc] = useState(props.ad[0].src);
    const [alt,setAlt] = useState(props.ad[0].alt);
    const [selectedBaner,setSelectedBaner] = useState(props.ad[0].key - 1);

    function selectBaner(baner: any){
        setSelectedBaner(baner.key - 1)
        setIsActive(baner.isActive)
        setUrl(baner.url)
        setSrc(baner.src)
        setAlt(baner.alt)
    }

    return( 
        <div className={`tabs-container`}>
            <div className={`articles-bar`}> 
                <div className={`articles-bar`} style={{minWidth:120}}> 
                    <Checkbox checked={isActive} onChange={()=>{
                        props.ad[selectedBaner].isActive = !isActive;
                        setIsActive(!isActive) 
                    }}  
                    color="primary" />
                    <p>{isActive ? 'Active' : 'Not Active'}</p>
                </div>
                <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Link'>
                    <Icon name='linkify' />
                    <input onChange={(e)=>{ 
                        setUrl(e.target.value) 
                        props.ad[selectedBaner].url = e.target.value;
                    }} value={url} />
                </Input>
                <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Source'>
                    <Icon name='image' />
                    <input onChange={(e)=>{ 
                        setSrc(e.target.value) 
                        props.ad[selectedBaner].src = e.target.value;
                    }} value={src} />
                </Input>
                <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Alt'>
                    <Icon name='sticky note' />
                    <input onChange={(e)=>{ 
                        setAlt(e.target.value) 
                        props.ad[selectedBaner].alt = e.target.value;
                    }} value={alt} />
                </Input>
            </div>
            <div className={`advertisements-view`}> 
                {props.ad.map((item: any)=>{
                    return (<img onClick={()=>selectBaner(item)} src={item.imgSrc} style={{opacity: item.isActive ? 1 : 0.5}} className={`baner`} alt={item.alt}/>)
                })}
            </div>
            <div className={`footer-articles-view`}>
                <Button onClick={()=>{ 
                    setSelectedBaner(selectedBaner)
                    setIsActive(props.ad[selectedBaner].isActive)
                    setUrl(props.ad[selectedBaner].url)
                    setSrc(props.ad[selectedBaner].src)
                    setAlt(props.ad[selectedBaner].alt)
                    props.saveAd() 
                }} className={`footer-btn`}> 
                    Save Changes
                </Button>  
                <Button onClick={()=>{ 
                    props.closeModal() 
                }} className={`footer-btn`}> 
                    Cancel
                </Button> 
            </div>
        </div>
    )
}
function Settings(props: any){
    const [isActive,setIsActive] = useState(false);
    const [url,setUrl] = useState('');
    const [src,setSrc] = useState('');
    const [alt,setAlt] = useState('');
    return( 
        <div className={`tabs-container`} style={{padding:10}}>
            <div>
                <p>Navigate Bar</p>
                <div className={`articles-bar`}>  
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Top Stories'>
                        <Icon name='pencil alternate' />
                        <input onChange={(e)=>{ 
                            // setUrl(e.target.value) 
                            // props.ad[selectedBaner].url = e.target.value;
                        }} value={url} />
                    </Input>
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='News'>
                        <Icon name='pencil alternate' />
                        <input onChange={(e)=>{ 
                            // setUrl(e.target.value) 
                            // props.ad[selectedBaner].url = e.target.value;
                        }} value={url} />
                    </Input>
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='About'>
                        <Icon name='pencil alternate' />
                        <input onChange={(e)=>{ 
                            // setSrc(e.target.value) 
                            // props.ad[selectedBaner].src = e.target.value;
                        }} value={src} />
                    </Input>
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Contact'>
                        <Icon name='pencil alternate' />
                        <input onChange={(e)=>{ 
                            // setAlt(e.target.value) 
                            // props.ad[selectedBaner].alt = e.target.value;
                        }} value={alt} />
                    </Input>
                </div>
            </div>
            <div>
                <p>Icons Link</p>
                <div className={`articles-bar`}>  
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Twitter'>
                        <Icon name='twitter' />
                        <input onChange={(e)=>{ 
                            // setUrl(e.target.value) 
                            // props.ad[selectedBaner].url = e.target.value;
                        }} value={url} />
                    </Input>
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Facebook'>
                        <Icon name='facebook' />
                        <input onChange={(e)=>{ 
                            // setSrc(e.target.value) 
                            // props.ad[selectedBaner].src = e.target.value;
                        }} value={src} />
                    </Input>
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Instagram'>
                        <Icon name='instagram' />
                        <input onChange={(e)=>{ 
                            // setAlt(e.target.value) 
                            // props.ad[selectedBaner].alt = e.target.value;
                        }} value={alt} />
                    </Input>
                </div>
            </div>
            <div>
                <p>Logo</p>
                <div className={`articles-bar`}>  
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Title'>
                        <Icon name='heading' />
                        <input onChange={(e)=>{ 
                            // setUrl(e.target.value) 
                            // props.ad[selectedBaner].url = e.target.value;
                        }} value={url} />
                    </Input>
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Icon'>
                        <Icon name='image' />
                        <input onChange={(e)=>{ 
                            // setSrc(e.target.value) 
                            // props.ad[selectedBaner].src = e.target.value;
                        }} value={src} />
                    </Input> 
                </div>
            </div>
            <div className={`articles-bar`}> 
                <div>
                    <p>Email To Submit</p>
                    <div className={`articles-bar`}>  
                        <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='email'>
                            <Icon name='mail' />
                            <input onChange={(e)=>{ 
                                // setUrl(e.target.value) 
                                // props.ad[selectedBaner].url = e.target.value;
                            }} value={url} />
                        </Input> 
                    </div>
                </div>
                <div>
                    <p>Popup Submit Time</p>
                    <div className={`articles-bar`}>  
                        <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Time To Render'>
                            <Icon name='time' />
                            <input onChange={(e)=>{ 
                                // setUrl(e.target.value) 
                                // props.ad[selectedBaner].url = e.target.value;
                            }} value={url} />
                        </Input> 
                    </div>
                </div>
            </div>
            <div>
                <p>Users</p>
                <div className={`articles-bar`}>  
                    <Dropdown
                        className={`footer-icon-form`}
                        // placeholder={props.mainStories[0].key}
                        fluid
                        search
                        selection
                        // defaultValue={props.mainStories[0].key}
                        options={[]} 
                        onChange={(e: any,opt)=>{  
                        }} 
                        icon={<Icon style={{opacity:0.5,marginRight:10}} name='list'/>}
                    />  
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='User Name'>
                        <Icon name='user' />
                        <input onChange={(e)=>{ 
                            // setSrc(e.target.value) 
                            // props.ad[selectedBaner].src = e.target.value;
                        }} value={src} />
                    </Input>
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Password'>
                        <Icon name='lock' />
                        <input onChange={(e)=>{ 
                            // setAlt(e.target.value) 
                            // props.ad[selectedBaner].alt = e.target.value;
                        }} value={alt} />
                    </Input>
                </div>
            </div>
        </div>
    )
}

function AdminModal(props: any){
    const store = useStore() 
    const [modalIsOpen,setIsOpen] = React.useState(true);
    const [verifyLogin,setIsVerifyLogin] = React.useState(false);
    const [userName,setUserName] = React.useState('avishay@soft.co.il');
    const [userPass,setUserPass] = React.useState('400x120x');
    const [mainStories,setMainStories]:any = React.useState(null);
    const [selectedStorie,setSelectedStorie]:any = React.useState(props.mainStories['mainStorie'] && props.mainStories['mainStorie'].item ? '0_index_' + props.mainStories['mainStorie'].item.title : null); 
    
    props.setAdminModeRefresh(false) 
    async function saveStorie(data: any) {
        props.setAdminModeRefresh(true) 
        console.debug('selectedStorie',selectedStorie)
        let index = selectedStorie.split('_index_')[0]; 
        alert(mainStories[index].item.index)
        let tableTitle = selectedStorie.split('_index_')[1]; 
        console.debug('selectedStorie',mainStories[index].item.tableName)
        if(mainStories[index])
            await store.updateStorie({
                mainTitle: mainStories[index].item.mainTitle.replaceAll('"',"_g_").split('\n'),
                subTitle: mainStories[index].item.subTitle.replaceAll('"',"_g_").split('\n'),
                mainText: mainStories[index].item.mainText.replaceAll('"',"_g_").split('\n'),
                subText: mainStories[index].item.subText.replaceAll('"',"_g_").split('\n'),
                category: mainStories[index].item.category.replaceAll('"',"'"),
                createdAt: mainStories[index].item.createdAt.replaceAll('"',"'"),
                readTime: mainStories[index].item.readTime.replaceAll('"',"'"),
                img: mainStories[index].item.img.replaceAll('"',"'").replaceAll(':','_d_').replaceAll(`\\`,"_b_"),
                index: mainStories[index].item.index.replaceAll('"',"'").replaceAll(' ',''),
                title: mainStories[index].item.title.replaceAll('"',"'"), 
                tableName: mainStories[index].item.tableName.replaceAll('"',"'").replaceAll(' ',''), 
            });
        closeModal()
    }

    async function addStorie(){
        props.setAdminModeRefresh(true) 
        await store.createStorie({
            mainTitle: 'mainTitle',
            subTitle: 'sub',
            mainText: 'textg',
            subText: 'subtext',
            category: 'category',
            createdAt: appService.getCurrentDate(),
            readTime: 'read',
            img: 'img',
            title: `new ${mainStories ? (mainStories.length + 1) : 1}`,
            index: `${mainStories ? (mainStories.length + 1) : 1}`,
            tableName: `newStorie${mainStories ? (mainStories.length + 1) : 1}`,
        });  
        closeModal()
    }

    function saveAd(){
        props.setAdminModeRefresh(true) 
        props.saveAd(); 
        closeModal() 
    }

    function deleteStorie(data: any) { 
        console.debug('deleteStorie')
        // let index = selectedStorie.split('_index_')[0];
        // console.debug('index' ,index) 
        // props.deleteStorie(parseInt(index)) 
        // setSelectedStorie('0_index_' + props.mainStories['mainStorie'].item.title)
        // getMainStories()
    }
  
    function getMainStories(){
        console.debug('getMainStories',props.mainStories.topStories.length,props.mainStories['mainStorieListUl'].length)
        const stories = props.mainStories;
        console.debug('mainStories',stories)
        if(!stories || !stories['mainStorie'].item)
            return null;
        let storiesOptions = []
        let index = 0;
        storiesOptions.push({
                key: `${(index + 1 ) + '. ' + stories['mainStorie'].item.title}`, value: `${(index) + '_index_' + stories['mainStorie'].item.title}`, text: (1 + index++ ) + '. ' + stories['mainStorie'].item.title, item: stories.mainStorie.item
        }) 
        for(let i = 0;i < stories['mainStorieListUl'].length;i++){ 
            storiesOptions.push({
                 key: `${(index + 1 ) + '. ' + stories['mainStorieListUl'][i].item.title}`, value:  `${(index) + '_index_' + stories['mainStorieListUl'][i].item.title}`, text: (1 + index++ ) + '. ' + stories['mainStorieListUl'][i].item.title, item: stories['mainStorieListUl'][i].item
            })
        } 
        for(let j = 0;j < stories['topStories'].length;j++){  
            storiesOptions.push({
                 key: `${(index + 1) +'. ' + stories['topStories'][j].item.title}`, value: `${(index) + '_index_' + stories['topStories'][j].item.title}`, text: (1 + index++ ) + '. ' + stories['topStories'][j].item.title, item: stories['topStories'][j].item
            })
        }
        setMainStories(storiesOptions)
    }
    useEffect(()=>{
        if(!mainStories || props.isOpenModal && !modalIsOpen)
            getMainStories()
        if(props.isOpenModal && !modalIsOpen)
            setIsOpen(true)
    },[mainStories,props.isOpenModal]) 

    async function closeModal(){
        await store.getStoriesList();
        getMainStories() 
        setIsOpen(false);
        if(props.mainStories['mainStorie'] && props.mainStories['mainStorie'].item)
            setSelectedStorie('0_index_' + props.mainStories['mainStorie'].item.title)
        console.debug('props.isOpenModal',props.isOpenModal)
        if(props.isOpenModal)
            props.setOpenModal(false)
        props.setAdminModeRefresh(false) 
    }
 
    return (  
        <Modal
          isOpen={modalIsOpen && props.isOpenModal}
        //   onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
        //   contentLabel="Example Modal"
        >
            {/* <button onClick={openModal}>Open Modal</button> */} 
            {/* <Icon onClick={closeModal} name='close' /> */}
            <div className={`admin-login`}>
                {!verifyLogin && <div className={`admin-login-form`}> 
                    <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='User Name'>
                        <Icon name='user' />
                        <input defaultValue={userName} onChange={(e)=>setUserName(e.target.value)}/>
                    </Input>
                    <Input className={`footer-icon-form grid-footer-item`} type='password' iconPosition='left' placeholder='Password'>
                        <Icon name='lock' />
                        <input defaultValue={userPass} type={'password'} onChange={(e)=>setUserPass(e.target.value)}/>
                    </Input> 
                    <Button onClick={()=> store.verifyLogin(setIsVerifyLogin, userName, userPass)} animated='vertical' className={`footer-btn`}>
                        <Button.Content hidden>
                            Now
                        </Button.Content>
                        <Button.Content visible>Login</Button.Content>
                    </Button>
                </div>}
                {verifyLogin && <div className={`admin-dashboard`}> 
                        <Tabs>
                            <TabList>
                                <Tab onClick={()=>{
                                    if(mainStories)
                                        setSelectedStorie('0_index_' + props.mainStories['mainStorie'].item.title)
                                }}>Articles</Tab>
                                <Tab onClick={()=>{
                                    if(mainStories)
                                        setSelectedStorie('0_index_' + props.mainStories['mainStorie'].item.title)
                                }}>Advertisements</Tab>
                                <Tab onClick={()=>{
                                    if(mainStories)
                                        setSelectedStorie('0_index_' + props.mainStories['mainStorie'].item.title)
                                }}>Settings</Tab>
                            </TabList> 
                            <TabPanel>
                                <div className={`admin-tab-view`}>
                                    {mainStories ? <Articles closeModal={closeModal} addStorie={addStorie} deleteStorie={deleteStorie} saveStorie={saveStorie} selectedStorie={selectedStorie} setSelectedStorie={setSelectedStorie} mainStories={mainStories}/> :
                                    <div>
                                        <div onClick={()=>{
                                            addStorie()  
                                            alert('New Storie Created!')
                                        }} className={`add-article`}>
                                            <Icon name='plus' /> 
                                        </div>
                                    </div>
                                    }
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <div className={`admin-tab-view`}>
                                    <Advertisements saveAd={saveAd} ad={props.ad}/>
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <div className={`admin-tab-view`}>
                                    <Settings/>
                                </div>
                            </TabPanel>
                        </Tabs> 
                    </div>}
          </div>
        </Modal> 
    );
}
export default AdminModal
// ReactDOM.render(<AdminModal />, document.getElementById('root'));