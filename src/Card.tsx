import * as  framer from 'framer-motion';
import { useEffect, useState } from 'react';
import './App.css'; 
function Card(props: any) {
    const [ open, setOpen ] = useState(false); 
    const CardData = props.data;
    useEffect(()=>{
        if(open){
          const scroll = document.getElementById('scroll')
          if(scroll)
            setTimeout(() => {
                const div = scroll.getElementsByTagName('div')[0]
                if(div)
                    div.style.overflow = 'hidden';
            }, 150);
        //   console.debug('hidden')
        }    
        else{ 
            const scroll = document.getElementById('scroll')
            if(scroll)
            setTimeout(() => {
                const div = scroll.getElementsByTagName('div')[0]
                if(div)
                    div.style.overflow = 'scroll';
            }, 150);
            // console.debug('unset') 
        }
        // console.debug('useEffect') 
    }) 
    return (
        <framer.AnimateSharedLayout>
            { open ? 
                    <framer.motion.div onClick={()=>{ 
                            // window.scrollTo(0, props.ulRef.current.offsetTop);  
                                if(!props.footer){
                                    props.setCloseCard(true);
                                    props.ulRef.current.scrollIntoView({
                                        behavior: 'smooth',
                                        // block: 'start',
                                    });
                                    setOpen(false)} 
                                }}
                                className='expanded-card'
                                layoutId="expandable-card" >
                                <div className={`card-container`}>
                                    <CardData/>
                                </div> 
                    </framer.motion.div> : 
                    <framer.motion.div onClick={()=>{
                        // window.scrollTo(0, props.ulRef.current.offsetTop);
                            if(!props.footer){  
                            props.ulRef.current.scrollIntoView({
                                behavior: 'smooth',
                                // block: 'start',
                            });
                    setOpen(true);}}}
                                className="normal-card"
                                layoutId="expandable-card" > 
                                        <CardData /> 
                    </framer.motion.div>
            }
        </framer.AnimateSharedLayout>
    )
}
export default Card;