import React, { useEffect, useState } from 'react'; 
import { GiSlashedShield } from 'react-icons/gi';
import { VscColorMode } from 'react-icons/vsc';
import { FiTwitter, FiFacebook, FiInstagram } from 'react-icons/fi'; 
import './App.css'; 
import './App.scss';
import 'semantic-ui-css/semantic.min.css'
import { ThemeProvider } from 'theme-ui'
import { useColorMode } from 'theme-ui'; 
import { BrowserRouter, Route, Switch, useHistory, withRouter} from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group'; 
import { Scrollbars } from 'react-custom-scrollbars';  
// import Card from './Card';
// import FadeIn from 'react-fade-in';
import Dropdown from './Dropdown';
import { Button, Icon, Input } from 'semantic-ui-react';
// import {useSpring, animated} from 'react-spring';  
import baner1 from './images/baner1.jpg';
import baner2 from './images/baner2.jpg';
import baner3 from './images/baner3.png';
// import { BLOCKS, MARKS } from '@contentful/rich-text-types';
// import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
import AdminModal from './Modal';
import { StoreProvider, useStore } from './stores/rootStore';  
import { Helmet } from 'react-helmet'

const TITLE = 'PreSolu'


interface ColorModeProps{
  setColorMode?: any;
  colorMode: string; 
}

const theme = {
  fonts: {
    body: 'system-ui, sans-serif',
    heading: '"Avenir Next", sans-serif',
    monospace: 'Menlo, monospace',
  },
  colors: { 
    background: '#fff',
    primary: '#33e',
  },
  light: { 

    app:{
      color: '#0f0f0f',
      background: '#fff',
    },

    galleryContainer: {
      gallery: {
        background: '#cccccc', 
      },
      imgTitle: {
        color: '#ececec'
      },
      galleryIcons: {
        fontSize:20,
        margin:10,
        cursor:'pointer',
        color: '#0f0f0f'
      },
      galleryLinks: {
        color: '#0f0f0f'
      },
      logoTitle: {
        color: '#000' 
      },
    },

    gridContainer: {
      gridIcons: {
        fontSize:20,
        margin:10,
        cursor:'pointer',
        color: '#0f0f0f'
      },
      gridText: { 
        color: '#0f0f0f',
        background: '#dddddd'
      },
      footerText: {
        fontSize:13,
        margin:10, 
        color: '#0f0f0f'
      },
      gridback: {
        backgroundColor: '#ececec'
      },
      footerTitle: {
        color: '#000' 
      },
    }

  },
  dark: {

    app:{
      color: '#ececec',
      background: '#111111',
    }, 

    galleryContainer: {
      gallery: {
        background: '#0f0f0f'
      },
      imgTitle: {
        color: '#ececec'
      },
      galleryIcons: {
        fontSize:20,
        margin:10,
        cursor:'pointer',
        color: '#cecece'
      },
      galleryLinks: {
        color: '#cecece',
        ':focus': {
          color: '#fff',
        },
        ':hover': {
          color: '#fff',
        },
      },
      logoTitle: {
        color: '#ececec'
      },
    },

    gridContainer: { 
      gridIcons: {
        fontSize:20,
        margin:10,
        cursor:'pointer',
        color: '#cecece'
      }, 
      gridText: { 
        color: '#cecece',
        background: '#0f0f0f'
      },
      footerText: {
        fontSize:13,
        margin:10, 
        color: '#cecece'
      },
      gridback: {
        backgroundColor: '#131313'
      },
      footerTitle: {
        color: '#000' 
      },
    }

  }
}

interface StorieProps {
  unikey: number;
  type: string;
  title: string;
  img?: string; 
}

const Storie = (storie: any) =>{  
  console.debug('storie.key',storie.unikey)
  if(!storie.unikey)
    return null
  switch(storie.type){
    case 'main': 
          setTimeout(() => { 
            const title = document.getElementById(storie.unikey + 'title')
            const sub = document.getElementById(storie.unikey + 'sub')
            const text = document.getElementById(storie.unikey + 'text') 
            if(title)title.innerHTML = storie.item.mainTitle;
            if(sub)sub.innerHTML = storie.item.subTitle;
            if(text)text.innerHTML = storie.item.subText;
          }, 0);
      return(
            <div className={`storie`} key={storie.unikey}>
              <div className={`storie-info-view`}>
                <div className={`storie-release-date`}>
                  <p>{storie.item.category}</p>
                  <p>{storie.item.createdAt}</p> 
                </div> 
                <div className={storie.unikey !== 1 ? `storie-title-c` : `storie-title`} id={storie.unikey + 'title'}></div>
                <div className={storie.unikey !== 1 ? `storie-sub-title-c` : `storie-sub-title`} id={storie.unikey + 'sub'}></div>
                <div className={storie.unikey !== 1 ? `storie-text-c` : `storie-text`} id={storie.unikey + 'text'}></div>
                <p className={`readmore-btn`}>READ MORE</p>  
                {/* <p>{storie.item.readTime}</p> */}
              </div>
              <div className={`storie-img-view`}><img onClick={()=>{
              }} className={`storie-img`} alt={storie.item.title} src={storie.item.img}></img></div>
            </div>
          )
    case 'main-child':
      setTimeout(() => { 
        const title = document.getElementById(storie.unikey*10 + 'title') 
        const text = document.getElementById(storie.unikey*10 + 'text') 
        if(title)title.innerHTML = storie.item.mainTitle; 
        if(text)text.innerHTML = storie.item.subText;
      }, 0);
      return(
            <div className={`storie-child`} key={storie.unikey}>
              <div className={`storie-child-top`}>
                <div className={`storie-info-view`}>
                  <div className={`storie-release-date`}>
                  <p>{storie.item.category}</p>
                  <p>{storie.item.createdAt}</p> 
                  </div> 
                  <div className={`storie-child-title`} id={storie.unikey*10 + 'title'}></div>  
                </div>
                <div className={`storie-img-view`}><img onClick={()=>{
                }} className={`storie-img`} alt={storie.item.title} src={storie.item.img}></img></div> 
              </div>
              <div className={`storie-child-bottom`}>
                  <div className={`storie-child-text`} id={storie.unikey*10 + 'text'}></div> 
              </div>
            </div>
          ) 
    default:
      return null
  }  
}

function GalleryContainer(props: ColorModeProps | any){
  const themeMode = props.colorMode === 'light' ? theme.light.galleryContainer : theme.dark.galleryContainer;
  const history = useHistory();
  const ulRef:any = React.createRef();


  const navigateTo = async(route: string) => {  
    if('/' + route !== history.location.pathname){ 
      if(ulRef){
        props.setGridAnime(true)
        ulRef.current.scrollIntoView({
            behavior: 'smooth', 
        });  
          history.push('/' + route,{ routeName: route }) 
      }
    }
  }; 

  return ( 
      <div ref={ulRef} className='gallery-container' style={themeMode.gallery}>
        <div className='gallery-view'>
          <div className={`gallery-col`} style={{alignItems:'flex-start'}}>
            <div className='gallery-header-left' style={{width:'100%',paddingTop:10}}>
              <a href='/' className='gallery-header-left-link'>
                <GiSlashedShield style={{fontSize:20,marginRight:5, color: themeMode.logoTitle.color}}/>
                <h3 style={themeMode.logoTitle}>insider journals</h3> 
              </a>
            </div>
            <div className='gallery-footer-left' style={{position:'absolute',top:'27vh'}}>
              <VscColorMode onClick={e => {
                props.setColorMode(props.colorMode === 'light' ? 'dark' : 'light') 
              }} className='gallery-footer-color' style={themeMode.galleryIcons}/> 
              <p onClick={()=>navigateTo('')} style={themeMode.galleryLinks} className='gallery-footer-link'>Top Stories</p>
              <p onClick={()=>navigateTo('News')} style={themeMode.galleryLinks} className='gallery-footer-link'>News</p>
            </div>     
          </div> 
          <div className={`gallery-mid-col`}>
            <div className="promo">
              <div className="image-wrapper"><img src="https://www.insiderjournals.com/wp-content/uploads/2018/04/Fotolia_189768487_Subscription_Monthly_M-1200x800.jpg"alt='gallery-img'/></div>
              <h2 className="title" style={themeMode.imgTitle} data-cta="The Full Guide →">Bitcoin Trading</h2>
            </div>  
          </div>
          <div className={`gallery-col`} style={{alignItems:'flex-end'}}>
            <div className='gallery-header-right'>
              <a href='https://www.twitter.com' className='gallery-footer-link'><FiTwitter style={themeMode.galleryIcons}/></a>
              <a href='https://www.facebook.com' className='gallery-footer-link'><FiFacebook style={themeMode.galleryIcons}/></a>
              <a href='https://www.instagram.com' className='gallery-footer-link'><FiInstagram style={themeMode.galleryIcons}/></a> 
            </div> 
            <div className='gallery-footer-right' style={{position:'absolute',top:'27vh'}}>
              <p onClick={()=>navigateTo('About')} style={themeMode.galleryLinks} className='gallery-footer-link'>About</p>
              <p onClick={()=>navigateTo('Contact')} style={themeMode.galleryLinks} className='gallery-footer-link'>Contact</p> 
            </div>   
          </div> 
        </div>
      </div>) 
}

 
 
const mainStories = {
  mainStorie: {key: 1, item:{ 
    img: `https://www.fairobserver.com/wp-content/uploads/2019/09/Finance-news-1.jpg`,
    title: `Storie 1`,
    createdAt: '14 hours ago',
    readTime: '2 min',
    category: 'Bitcoin',
    mainTitle: `<h1 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading guide.</h1>`, 
    subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
    subText: `<ul>
      <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
      <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
      <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
      <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
      </ul>`,
    mainText: `<ul>
      <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
      <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
      <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
      <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
      </ul>`,
  }},
  mainStorieListUl: [
    {key: 2, item:{ 
      img: `https://www.insiderjournals.com/wp-content/uploads/2018/05/Fotolia_184045725_Subscription_Monthly_M-1024x504.jpg`,
      title: `Storie 2`,
      createdAt: '14 hours ago',
      readTime: '2 min',
      category: 'Crypto',
      mainTitle: `<h3 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading guide.</h3>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `Like stocks and forex, you must have a well defined technique or strategy for trading Bitcoins too. This implies that you must know exactly when to enter or exit a trade position, how much you `,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
    }},
    {key: 3, item:{ 
      img: `https://www.insiderjournals.com/wp-content/uploads/2018/05/bitcoin-google-1-1024x569.jpg`,
      title: `Storie 3`,
      createdAt: '12 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h3 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading guide.</h3>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `Like stocks and forex, you must have a well defined technique or strategy for trading Bitcoins too. This implies that you must know exactly when to enter or exit a trade position, how much you `,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
    }},
    {key: 4, item:{ 
      img: `https://assets.vccircle.com/uploads/2017/06/au-finance-ipo-story_thinkstockphotos-607900634.jpg`,
      title: `Storie 4`,
      createdAt: '15 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h3 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading guide.</h3>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `Like stocks and forex, you must have a well defined technique or strategy for trading Bitcoins too. This implies that you must know exactly when to enter or exit a trade position, how much you `,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
    }},
    {key: 5, item:{ 
      img: `https://www.insiderjournals.com/wp-content/uploads/2018/05/bitcoin-google-1-1024x569.jpg`,
      title: `Storie 5`,
      createdAt: '14 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h3 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading guide.</h3>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `Like stocks and forex, you must have a well defined technique or strategy for trading Bitcoins too. This implies that you must know exactly when to enter or exit a trade position, how much you `,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
    }},
  ],
  topStories: [
    {key: -1, item:{ 
      img: `https://www.insiderjournals.com/wp-content/uploads/2018/05/bitcoin-google-1-1024x569.jpg`,
      title: `Storie 6`,
      createdAt: '14 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h1 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading guide.</h1>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
    }},
    {key: -2, item:{ 
      img: `https://ssl.gstatic.com/finance/favicon/finance_496x496.png`,
      title: `Storie 7`,
      createdAt: '14 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h2 class="entry-title">How to Right The full Bitcoin full Bitcoin trading.</h2>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `Like stocks and forex, you must have a well defined technique or strategy for trading Bitcoins too. This implies that you must know exactly when to enter or exit a trade position, how much you 
      Like stocks and forex, you must have a well defined technique or strategy for trading Bitcoins too. This implies that you must know exactly when to enter or exit a trade position, how much you `,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
    }},
    {key: -3, item:{ 
      img: `https://cdn.smefinanceforum.org/sites/default/files/shutterstock_1774019165-r.png`,
      title: `Storie 8`,
      createdAt: '14 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h2 class="entry-title">How to Right The full Bitcoin full Bitcoin trading.</h2>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `Like stocks and forex, you must have a well defined technique or strategy for trading Bitcoins too. This implies that you must know exactly when to enter or exit a trade position, how much you 
      Like stocks and forex, you must have a well defined technique or strategy for trading Bitcoins too. This implies that you must know exactly when to enter or exit a trade position, how much you `,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
    }},
    {key: -4, item:{ 
      img: `https://www.insiderjournals.com/wp-content/uploads/2018/05/Fotolia_184045725_Subscription_Monthly_M-1024x504.jpg`,
      title: `Storie 9`,
      createdAt: '14 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h1 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading guide.</h1>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `<ul>
        <li>In fact it is one of the widely traded cryptocurrencies today.</li>  
        </ul>`,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
    }},
    {key: -5, item:{ 
      img: `data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFRgVFRUYGBgYGBgYGBgYGBERGBgYGBgZGRgYGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHxISHjQrJCs0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NjQ0NDQ0NDQ0NDQ0NDQ0NDE0NDQ0NDQ0NDQ0Ov/AABEIALIBGwMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAADBAIFBgABB//EADgQAAICAQIEBAQEBQQCAwAAAAECABEDBCEFEjFBIlFhcTKBkbETQqHRBiNSwfAUcpLhgvEkM1P/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQMCBAX/xAAjEQADAQADAQACAgMBAAAAAAAAAQIRAyExEgRBIjITUXFh/9oADAMBAAIRAxEAPwDZmKatbEcgsiXINaUTMfxPRljD8O0XLLzLpbksWCoSsBvTzFh2k/wBGFE4xiALiEmBJmRMAPCZB3qSJi+TrEMZwt3jKNcrQ1bRrA80jLH1QSDqJJGhggM1gFFxPQhwdpiNXoWxP6GfUmwXM/xzhvMDQ9pDljrUX4eT5eMy2neOo0STGVNEURG8U48O4KBCphJksKS20mETUzoqrBTT6AmWun0VRrEgEZQSqlEapnafDULqXpZ6pqVPFdZW002pRhJ0wWIkuTLnBKXQefnLfCbj4ifIPIYRhtA4ljAnUiDM/wAQSnvsYqTLPiybXKi4mAUGTDQIMkpgAcGSUwSmEUxoQZTJ3AqYS4xA54ZxnhMmbPCJGp6TIkwA9uRJnhaeXEBxMiTPSDIPtE2NI8ZoFGuLajU1tJ432uZmlTxGqlpBS285Mm8gTtcXV95owX2DMNo1jazM/hz20utKRsZRPRFqibQWXTBhRnuPLDLvBoDHcd4WR4lHSU6JPo2owhhRmQ4roeRrA2nLy8ePUdfDy6vliWMSy0eSIokbwYt5OS7LjG0MrRXCJ7my0JXSOdhNTqgomZz5i7z3i2u2IuL8KQsbkm/p4bz5Wmg0K0AJaYInpMBlpiw1OmJxHJdaw6CFBghtJyyJC2sxcwIlDnwMp9JphvFNThBjAz4MIrSwfSgwTaSLAFg0KpnHTHtI8pHWMQZTCXAoZK4ABXLJF5VYdVYljpMRcXJKtN4T5rklxExvDpgI0FE1gFcuCEOGMsu8iwhgtE2WJ6tqj+QRLUJcWBpndc/eN6V7FTzXaYnwqLJ2AHcxnRcG1C0Sqj05hcgl80V7qQpWliRPWWj6DLXwH5EGI6jR5FB8D/JSftKtoxjAYH3ljptT2uU+F/r9IbCxuCYmjV6bIPOWCvtMnj1RBl1o9TYlExFkpi+r04cdJx1Ak0fzg1oJ4ZnPgKN02hcby61eAOJXDRVOauNp9HVPKmuyBz1K7V6knYS0OhjWDhq10guOmD5pXhk04Q+Rrbp5TTaDhioAKj2DAFNVGMm0rPEpI1y1RBMfLG1FiQUWLnqN2lcJHX2noMg43nhMYBIvnhg1RXUZIABLTvxIB8sgHszLYx9BcmdOD2naZdoZjUaEVubS1uIvcvFSxANohGIxnCdKzTVaPFyiV3DkCgS2R5Hjno3TJuJCHQXBZElcMnAQbTxMnaL67U0ypfXxN7dh89/pMVSlaamXTxE3xseik/KJZ1ZT4gR7iXOHUWIbmDCjRHkd5hWUfGZrSOC5Plt+8szqahcnCVFlPCfL8v8A1KTXF0flfa+hnPf0nrOqJmkki2TWiGTViZpc9GiYQaj1mVZt8KNHkwJlHiRW9xv9esrNT/Do3bGSD/SxsH0B6iF4bqt6uXa5NpaXqOe4x4YByysVYEEGiD2lhps5A2lh/EeFSv4gHiWg3qvr7fvM8+XyMVc3y8Q+P8b67b6LjFqt9yCfeH5tRk2RkQefiyN+oAEo8O3SW2l1QTcmpj/M3/Zlf8Ez2kXWHSOqi3LnvYXf6AVIHeBXjKEGmB27Bn8v6QfMfWKYNWOb4hv0sgE+wO/6S8c0eacnJD3pFsAJPC8Ar7TzA28smRD5jRhH3EV1OSED+GaALhftIq3ig8LT1esyAV3klgQbMYKzQHZDK3UvGsryq1jwAV1OXrC6BSTEHJJlvw9aFyfrH+i0Vgonq+KLMxNCOYloSggybSViKZM28IGgBS402hkciQRxGxjsTMobC4HhGieDJRox6aEVep8LCU/4pd2fsTS/7RsP3+cveJYyUYqCSFNUCTdekosaUoHcVOXme9I6vx5zWMLr6NfKPYdaPOU3FcXLTjo1BvRux+cTTVETn+nLOtQqWo3GDUgztfo0zKVcbdiNmU+antMzoeIURZmo0mUMBLTStYQuHD1GC4tpM2ncBxzoxpci7fJl7NPGx5AD4TuO2/2n0TUaVMilXFqe32I8jMZxZjp2Ktufyeo7GYrhla2+ii/JuslLWKaHVMrDmBHvYms0mbnU0d6Ne9bT5pk1T8/Pdnv6+k0vAuKgEb7H9D5GT47x/wDhfl4nU7+wClySwssSyujE79mU+v2iL7bb/PY+x9Zp9Roi2T8TGRyvu4uqYfmHv/aD/iPBiw40zP8AEDv+YNQ2tR1ANHr0Bmrnr6/0Snl+OmU2mxcviyHfl5gnwgKK8bkG+jAhBRO24BF+ajkBtt3BV0oghWAPhIB5VGxsUbsX0nmbMy+JWfnazZ2BVhfMARYJJPqBYizYsS0WYJYtQz4xYBr4TW2zf4N+OqbfROrbess8Gdip/DRQOQqNyStLR5Tfly2OlrJprr2KDZeUAMy0bJ3DXYOwr95UYeIpz5R+MvKoQ4wXTb+S4aj0J5it13htBq1dMZLh3ZQHFrzBh3Iuye3TtG+N5pP7L5CQC2NjygKWVvDQPXw3t7rY8wY9w/UB7rqDTKeqnyP794DDhob+QKsOoIIojz9vfyIi7n8JxkUbliHVQBYO9LQ3sAlfIqe1zo4brjffhOsr/pa6jcybtQqLqeZ/MdQR0IO4I9CKksnUT0U97IDWP4Z7ikGO1RvT49owPUTvPHyVCOaibmzNCPMx2lPrXlrqWoSh1T7zNPoaI4N2l/p0pZT6HFe8vsI2ilAyeBO87U5aFCRzZgolajlz85sQ1iJYyxVYDT4qjUEBmw1ES50xsSjfJvH9JqZiRsebB3iuj1PO7D8qHlHqR8R+sPqtaERnP5Rt6noo+tSj4Rk5VBJ69/M95PmvGkW4Y1NmsQiL6zQLk3qm7MP7+cHj1IqMplBgsaH3L0z+r05UFHH9wR6TM5NKytQ3HYHt7GfSs2FXXlYWPt6gzIcf4VkxqWTxL3P5lXvt/ec/Lxs6uDlW4yiTGxvkN1tLzgXEjfK2xBoiKcNRSB5yev0pH8xPiXqP6l7/ADEyp+e0Vpq/4s2a5tr9LmT/AIi/+QpHLyulld750/Mo9aHMB6GWPA+IB1o9xU7/AEnMxT86HYjr5qw/ztLvLk5knx13+j5+cUZ0Wma7U15yz4xw4pk3Wg3iFChf5q+9drnuiwbzkc48O77TnUWHDseQblgFG/nK/imTJkyYwGI5WLEAqGKkBFAsjqWYH/dHeIasoAi+7H7CAsN+Ex5q5j05r5ucHY9AQvMd+01yTkHDXJ9UxV8Vnp69SxC9gSfShJPpEcEZEU0j8lhSQeUkAGthe/bc7dDT2sDItqnN0DV+Ubm677gfW+0XRy2N25gdzuAf/wA3/N/acyn5pGN1Gf8A4X0SOmQuisVK0SAeUcrknofIdPKd/EGlxJyPjHIxLWo6V+Uj96Htsbh/D+l51c87puq+BuQG1c+LY/0/rD8U4YEU5Fy87KbIY87UCi8wat93XavWdKecj7Jvw1HA9SzYgHNvygGz4r2IJHfw8pPe5LVMpBDDYqeh6lfEt/MV/wCZlTwLiPOhLcqtzmzarzsVtTv3PKbrbYbbkiz1OBkYO5u26DsCpI69RJ3TW6OVo/wo/wApDRHKGSjuQFY8gvv4CgjCGzFdM3g9CxPYflQdvY/SOadPOehwvYRGl/JjWJLMa5qEUR56WJlkZJPk3nVBCGWMBXXGlmcytZqaDiQ8JmbRvFMUNF1wpbEsMz8ouJ8MbaQ4k5JoRz4D9PGcvtHdJgqC0WChv1jgNRoQ3jkriivDRgZh1kEylTGZ5kwWJjBi/FdVzKiDuSx+Ww+5nabGWxkA0w3X9jEcqVkryUfrce0j1Oav5U9OyP4wsOwcRsb7EEgg9QRLTSa2+8ouL6UHxps3fyPvENFxBkNHqO1ybpyyyhVOo+i6bNcbKWPSZfRcRBqjNHpstidEUqOW5cmX12hGPIQopW8S+nmJJBtLjjeMEA9x9j1lRz0JOl8stNNyhH/RvjfnxixdlO49V/aWeDiCswbxK4FdKNeR2hsQ2uHAB6wSzxjqvrpoqeO8+XkCqWC2SSd7NdB8p5pdKVFkUZdY9PR2MhmSanj+q+mS5OVzPzJTZNJfWVnFcBXHYIXlbmLlA5VD8bAeY8J9AjHtNMMVweXTBgVPce/6d/buLE3y8f1OHPNYyt02cZEXIl0e5uydub16+fYyv4+HR7wY0POrq5YhRZAFgEi2q96v9YYlsDHwEr0qweWksHsSPAaNVQ3PXlhrdVzWVbmFAk+KvEAQG8jv38/WebTz30thm+G/6nD+Ii4Q3Tn5mUFfASK368pJ2nmq/wBTmRWKBEbe7vmoDc1bHop6eVy9xFCXdnpn5SwJBBKJyeHyu779Iq2TkVES6RAos7kDz99u0HzLdSWmlJ2gwfh4yijmfxMbKpZA6Wdh8Nef2jeDnOLme+c0UC0ByhwrUD1osqedmQwK5NuSN1tCSHcHx7X8Iq25m613Il1wvR85GVlrlX+Wu5ryYXvyDblB3vfyvMRXJWf7CqUoMuOqQflG/fxEktv38RMdQGpNcQFbTn2nsRCmUkcrevT1RJNOQyOQzYjkh1EWR4RMsAIa9LUzLOKaa7OLWZLULTn3mKGi64am0YfHbwfDztC5XAjXgmHRxJFoniez6RzHNAepDwZEJUAM8m8bHSLaaj9IYkDqdgLv2mQKZxzZ39CB9AL/AFlljxgDeVXCXLsWr4mJ+puaTHpiepnIu22dz/ikij4hzcp5R/noJn9NpGDEnqTZ+c3mbTCVeo0YuxMXL3S/FyJLBF9OyoMidviHp5iaXgevDqN94lpEoVF20pwPzoDyE+JR+X1Hp9pqX8vUYtKlj9/Rpc2AZAy341IYfTYex3lFmx73+nl5iOZNVysmdfEDSOBueW9j8iZLVKGYsvQ7+W/eVrKWohGy8ZPSqKhHSC02M15RxKEyu0NvH0ATIeYKdoV1i2uBDBx84wH5gCJvieU0zHNOyqQJjUHW8OyQbip0HMB1OlVxTfIirF9fceh2me1/BcoYOjBlXmPJZxqSx5uZ6N7Gj+bp2miyZgqlmNACyZlddr3znlsqnZR39W/acn5Exnfp0cEVb68FtKTX8xkdjXMy/gKQwO1BiPDQUHofi232s8OnZweRV5mC34saoCOXoUUtVg7bdBK/HpUBFATScNAHWcU8e0dtcSlektLwPlAfK3OR2ICpexsqPiNqDbXvvQMsWMk+qU+BTvsW9B6+8XyNPR4omV0edyeki8g5ucV2keWzKmCSHaRd5JEoTmWaEQUyWLrOahOxHeADpTaZbX4v5k1SttKDieM84MzXg0NaBaWBzi3qNaVfDB0Oa4/0I9xr0jHNUXOSpMNcYDmI3GItiENcBGa05h84JR67o1f8TFEyb7R7GwqvMzPqw14yg4SxFVNVgckTKaIcjsv9LEfQzUaR7AnJJ3PtaH5SZE4rjaJDLgubc6YVYILi9I0uOof8KpxSOZwzV6Lrpk7KPsPpIuBdRwDaJZtjG1gk9Zxadz1BE7QLP2k2zaWhme9jDYRUTxtGRkhL/Zql1hI6gXR2gsupQCy6j5iVXFtVS7dZQuxPXrCvyGusCfxFS3cGOL8WORyibIN/VjfU+kRwxdvj91+0ZQTmdVVbR2TMxPyhvTGyBH9SuVnTFh2JHM7noi9B8zvI8K0hY32mlXHQ95fhjX2c3Py/K6FdNplxqFX5k7lj3JPnJEybmQAnbh5+72TA7T0LBK+8KzRiPC09qCU2Yfk2gAtlnuESTrPUgA5i3iPE8MbwmD1w2gAHSDwxDUkh6EfQ8qyr1GW3i8QDCHeNYwOsSxjvG8SXGA3zeUmsHjWoaAGQXHv7RvGx+YiaZYziy9ZiRsQ1G2oJ7OFb9KP2mm4aLAmd4oo50I7qR9D/ANy94Vl2E52spnVL2EX+PEIwFgdO0YuXRJg3WeMu0IwkWmcDQJWI6gSwbvEs25irwc+ijLANGWg3xyTLoEBUL2PtDDFYgsfWGYH1pQuvOGHfcSvZLHrLXUYirvXUHcRdsV9JH5L/AGU2XGT7jvD6W73EcGAk1Vy54fwkDxOPl+81MOniMXzKV2McMQ1sKHn+0sGecNpBzO2IUo8+7dMBkM8uTMg6zZkGJIvc9IkVEACY4zzQKKJMdIACffecghuXrIlYAFwdYTMliDwiNLACq1IpSJTYzZJ9ZoOJJ4TMxhbr7mZoaLHE1x/AaqVOnePoxsRoTLFYehFEMY5TGBhQ1fWPIoq5X4sd7RkFlU0em/ntJJmmKavVc2VU/oAP/L/1L7hz7iZzWY+XOtvzM6WenUEX095d6F6qQe/XZ0xnz0azTPHUMp9C9y1xGWlk6QeoMiTBnhmmYQvn6H2iLG4/qBtEFk69Kz4BYSarJkT3liUjdBcKRbNipj9Y0jz3KLo/IzTXRlPsqdbp7YOO4o+/aLLg3qqb7y0ddmQ/I/Yz1EoetdZNcf0zVcnysBaXSqu5AJ+3tGHWRUmS5p0zKlYjmqnT1kZ43SSuRYTQgDSBMmVnlQADkbpJYh1nmQSQuABEO8Y5Yui7xlGgBwWcRJMZECAEscOsAoh1gApxFPCZl0IBI9ZrNatrMYT42HrMsEO4hvLLGZWoaEsMJuNDY7gaM/iRHFcajEYrSdf884fL8L+x+06dJT4afopqkF4jQvmO9C/gHeP6aezpGv7M6OP+pfcOl3hnTpuPBcnowJxnTpUkB1HT6feInvOnSdelZ8IiEE6dBGWTEji6GdOgxLwHn6r7SSTp0rPhKvSJns6dNCINPGnToADMgO06dADm6mer0nToAcIYdJ06AHohFnToASEIes6dAAWp+GYxv/uf3nTpmgQwv9/7yx0s6dGhsexw86dGI//Z`,
      title: `Storie 10`,
      createdAt: '14 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h1 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading guide.</h1>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `<ul>
        <li>In fact it is one of the widely traded cryptocurrencies today.</li>  
        </ul>`,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
    }},
    {key: -6, item:{ 
      img: `https://www.investopedia.com/thmb/CWw6S3djV26V_Qhxpd7RJaT4KY8=/3484x2613/smart/filters:no_upscale()/bitcoin-mining-software-5b73752fc9e77c0057beb28f.jpg`,
      title: `Storie 11`,
      createdAt: '14 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h1 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading guide.</h1>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `<ul>
        <li>In fact it is one of the widely traded cryptocurrencies today.</li>  
        </ul>`,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
    }},
    {key: -7, item:{ 
      img: `https://static.wixstatic.com/media/6b36fa_0394ddbdbf554889ba45e87e6dadefea~mv2.jpg`,
      title: `Storie 12`,
      createdAt: '14 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h1 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading guide.</h1>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br>
        <li>In this article, we shall reveal some of the best and profitable strategies for&nbsp;<strong>investing</strong>&nbsp;in Bitcoin.</li><br>
        <li>If you are new to crypto trading, then continue reading to find our comprehensive guide to&nbsp;<strong>profiting</strong>&nbsp;from Bitcoin trading.</li>
        </ul>`,
    }},
    {key: -8, item:{ 
      img: `https://www.insiderjournals.com/wp-content/uploads/2018/05/bitcoin-google-1-1024x569.jpg`,
      title: `Storie 13`,
      createdAt: '14 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h1 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading.</h1>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br> 
        </ul>`,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br> 
        </ul>`,
    }},
    {key: -9, item:{ 
      img: `https://www.insiderjournals.com/wp-content/uploads/2018/05/Fotolia_184045725_Subscription_Monthly_M-1024x504.jpg`,
      title: `Storie 14`,
      createdAt: '14 hours ago',
      readTime: '2 min',
      category: 'Bitcoin',
      mainTitle: `<h1 class="entry-title">How to profit on Bitcoin? The full Bitcoin trading.</h1>`, 
      subTitle: `<p><strong>Are you looking to make money from the comfort of your home?&nbsp;</strong></p>`, 
      subText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br> 
        </ul>`,
      mainText: `<ul>
        <li>Needless to mention, Bitcoin is the buzz word these days. In fact it is one of the widely traded cryptocurrencies today.</li><br>
        <li>Despite the regular ups and downs in its prices, the currency continues to dominate other cryptos in the world of trading.</li><br> 
        </ul>`,
    }}, 
  ] 
}
  

function ListContainer(props: any) { 
  const themeMode = props.colorMode === 'light' ? theme.light.gridContainer : theme.dark.gridContainer;   
  const [listData,setListData] = useState([])
  const store = useStore() 

  useEffect(()=>{
    buildListData(store.stories.topStories) 
    if(props.isAdminModeRefresh)
      buildListData(store.stories.topStories) 
  },[props.isAdminModeRefresh])
  const buildListData = (data: any[]) =>{
    let listData: any = []
    data.forEach((item,index) => {
      console.debug('buildListData  ',!index || !(index % 6),index === 1 || !(index % 7),data[index].key)
      if(!index || !(index % 6))
        listData.push(<div key={index} className={ `list-item-0`}>
          <Storie type={'main'} unikey={item.key} item={item.item}/>
        </div>)
      else if(index === 1 || !(index % 7))
        listData.push(
        <div key={index} className={data[index+2] ? `list-item-1`: `list-item-1-row`} style={!data[index+1] ? {height:400} : {}}>
          <div className={data[index+2] ? `list-item-1-left`: `list-item-1-left-row` } style={!data[index+1] ? {height:400} : {}}>
            <div className={`list-item-1-left-t`} style={data[index+2] ? {} : {height:300}}>
              <Storie type={'main-child'} unikey={data[index].key} item={data[index].item}/>
            </div>
             {data[index+1] && <div className={`list-item-1-left-t`} style={data[index+2] ? {} : {height:300}}>
              <Storie type={'main-child'} unikey={data[index+1].key} item={data[index+1].item}/>
            </div>}
          </div> 
          {data[index+2] && <div className={`list-item-1-right`}>
            {data[index+2] &&<div className={`list-item-1-right-t`}><Storie type={'main'} unikey={data[index+2].key} item={data[index+2].item}/></div>}
            {data[index+3] &&<div className={`list-item-1-right-t`}><Storie type={'main'} unikey={data[index+3].key} item={data[index+3].item}/></div>}
            {data[index+4] &&<div className={`list-item-1-right-t`}><Storie type={'main'} unikey={data[index+4].key} item={data[index+4].item}/></div>}
          </div> }
        </div>)  

    })
    setListData(listData)
  }
  
  return ( 
    <div className={`list-container`} style={themeMode.gridback}>    
          <div className={`list-stories`}>{listData}</div>   
    </div>
  );
  
}
 

function GridContainer(props: ColorModeProps) { 
  const themeMode = props.colorMode === 'light' ? theme.light.gridContainer : theme.dark.gridContainer;   
  const [isCardOpen,setCloseCard] = useState(false)
  const [stories,setStories] = useState([])
  const store = useStore() 
  
  useEffect(()=>{ 
    if(isCardOpen)
      setCloseCard(false) 
  },[ isCardOpen])
 
  return ( 
    <div className="grid-container" > 
         <div className={`grid-top-stories`} style={themeMode.gridback}> 
        {/* <h1>We have another good strategy for crypto traders who wish to trade Bitcoin. Yes! Opt for basket trading. In this strategy you can swap old cryptocurrencies including Bitcoins with the new ones. This will fetch you higher profits and greater returns</h1> */}
         
            <div className={`grid-top-stories-main`}> 
               <Storie type={'main'} unikey={store.stories.mainStorie.key} item={store.stories.mainStorie.item}/>
            </div>
            <div className={`grid-top-stories-section`}>
               <ul className={`section-stories`}>{store.stories.mainStorieListUl.map((storie: any,index: any)=>{
                return (
                  <li>
                    <Storie type={'main-child'} unikey={storie.key} item={storie.item}/>
                  </li>)
              })}</ul>
            </div> 
        </div>
    </div>
  );
  
}

function InfoScreen(params: any) {
  const [colorMode, setColorMode] = useColorMode(); 
  const [infoData, setInfoData] = useState(params.props.location.state)   
  const themeMode = colorMode === 'light' ? theme.light : theme.dark; 
  const history = useHistory();
  const ulRef:any = React.createRef();

  const navigateTo = async(route: string) => {  
    if('/' + route !== history.location.pathname){   
          history.push('/' + route,{ routeName: route }) 
    }
  }; 

  console.debug('InfoScreen')
  return ( 
    <div className='App' style={themeMode.app}> 
      <div className='gallery-container' style={themeMode.galleryContainer.gallery}>  
        <div className='gallery-view'>
            <div className={`gallery-col`} style={{alignItems:'flex-start'}}>
              <div className='gallery-header-left' style={{width:'100%',paddingTop:10}}>
                <a href='/' className='gallery-header-left-link'>
                  <GiSlashedShield style={{fontSize:20,marginRight:5, color: themeMode.galleryContainer.logoTitle.color}}/>
                  <h3 style={themeMode.galleryContainer.logoTitle}>insider journals</h3> 
                </a>
              </div>
              <div className='gallery-footer-left' style={{position:'absolute',top:'27vh'}}>
                <VscColorMode onClick={e => {
                  setColorMode(colorMode === 'light' ? 'dark' : 'light') 
                }} className='gallery-footer-color' style={themeMode.galleryContainer.galleryIcons}/> 
                <p onClick={()=>navigateTo('')} style={themeMode.galleryContainer.galleryLinks} className='gallery-footer-link'>Top Stories</p>
                <p onClick={()=>navigateTo('News')} style={themeMode.galleryContainer.galleryLinks} className='gallery-footer-link'>News</p>
              </div>     
            </div> 
            <div className={`gallery-mid-col`}>  
            </div>
            <div className={`gallery-col`} style={{alignItems:'flex-end'}}>
              <div className='gallery-header-right'>
                <a href='https://www.twitter.com' className='gallery-footer-link'><FiTwitter style={themeMode.galleryContainer.galleryIcons}/></a>
                <a href='https://www.facebook.com' className='gallery-footer-link'><FiFacebook style={themeMode.galleryContainer.galleryIcons}/></a>
                <a href='https://www.instagram.com' className='gallery-footer-link'><FiInstagram style={themeMode.galleryContainer.galleryIcons}/></a> 
              </div> 
              <div className='gallery-footer-right' style={{position:'absolute',top:'27vh'}}>
                <p onClick={()=>navigateTo('About')} style={themeMode.galleryContainer.galleryLinks} className='gallery-footer-link'>About</p>
                <p onClick={()=>navigateTo('Contact')} style={themeMode.galleryContainer.galleryLinks} className='gallery-footer-link'>Contact</p> 
              </div>   
            </div> 
          </div>  
        </div>
        <div className='info-view'>
          <div className='info-container' style={themeMode.gridContainer.gridback}>
            <h1>{infoData.routeName}</h1>   
            <div className='footer' style={themeMode.gridContainer.gridback}>  
                <div className='gallery-header-left'>  
                    <p style={themeMode.gridContainer.footerText}>developered by Avishay Tal</p> 
                </div>
                <div className='gallery-header-right'>
                  <a href='https://www.twitter.com' className='gallery-footer-link'><FiTwitter style={themeMode.gridContainer.gridIcons}/></a>
                  <a href='https://www.facebook.com' className='gallery-footer-link'><FiFacebook style={themeMode.gridContainer.gridIcons}/></a>
                  <a href='https://www.instagram.com' className='gallery-footer-link'><FiInstagram style={themeMode.gridContainer.gridIcons}/></a> 
                </div> 
            </div>
          </div>
      </div>
    </div> 
  );
  
}


function MainScreen(params: any) {
  const [colorMode, setColorMode] = useColorMode(); 
  const [gridOpacityAnime, setGridAnime] = useState(false); 
  const [isReady, setIsReady] = useState(false); 
  const [isOpenModal, setOpenModal] = useState(params.admin ? true : false); 
  const store = useStore() 
  const [ad, setAd] = useState([
    {key: 1, url: 'https://google.com', imgSrc: baner1,isActive: true, alt: 'baner-1'},
    {key: 2, url: 'https://facebook.com', imgSrc: baner2,isActive: false, alt: 'baner-2'},
    {key: 3, url: 'https://twitter.com', imgSrc: baner3,isActive: true, alt: 'baner-3'}, 
    {key: 4, url: 'https://instagram.com', imgSrc: baner2,isActive: true, alt: 'baner-4'},
  ]); 
  const [isAdminModeRefresh, setAdminModeRefresh] = useState(false); 
  const themeMode = colorMode === 'light' ? theme.light : theme.dark;   
  
  // function addStorie(){  
  //   console.debug(`addStorie before ${ mainStories['topStories'][mainStories['topStories'].length - 1].key - 1}`)
  //   mainStories['topStories'].push({key: mainStories['topStories'][mainStories['topStories'].length - 1].key - 1, item:{ 
  //     img: `https://www.insiderjournals.com/wp-content/uploads/2018/05/Fotolia_184045725_Subscription_Monthly_M-1024x504.jpg`,
  //     title: `Storie ${mainStories['topStories'].length + mainStories['mainStorieListUl'].length + 2}`,
  //     createdAt: '',
  //     readTime: '',
  //     category: '',
  //     mainTitle: ``, 
  //     subTitle: ``, 
  //     subText: ``,
  //     mainText: ``,
  //   }});
  //   console.debug(`addStorie after Storie ${mainStories['topStories'].length + mainStories['mainStorieListUl'].length + 1}`)
  // }

  function saveAd(){
    console.debug('ad',ad)
    setAd(ad) 
  }

  // function deleteStorie(storieIndex: number){
  //   let newStoriesOptions:any = {}
  //   let index = 1; 
  //   console.debug('deleteStorie storieIndex',storieIndex)
  //   if(storieIndex === 0){
  //     newStoriesOptions['mainStorie'] = mainStories['mainStorieListUl'][0];
  //     newStoriesOptions['mainStorieListUl'] = [];
  //     for(let i = 1;i < mainStories['mainStorieListUl'].length;i++){ 
  //       newStoriesOptions['mainStorieListUl'].push(mainStories['mainStorieListUl'][i])
  //     } 
  //     newStoriesOptions['topStories'] = [];
  //     for(let j = 0;j < mainStories['topStories'].length;j++){  
  //       newStoriesOptions['topStories'].push(mainStories['topStories'][j])
  //     } 
  //   }else {
  //     newStoriesOptions['mainStorie'] = mainStories['mainStorie'];
  //     newStoriesOptions['mainStorieListUl'] = [];
  //     for(let i = 0;i < mainStories['mainStorieListUl'].length;i++){ 
  //       if(storieIndex !== index)
  //         newStoriesOptions['mainStorieListUl'].push(mainStories['mainStorieListUl'][i])
  //       index++;
  //     } 
  //     newStoriesOptions['topStories'] = [];
  //     for(let j = 0;j < mainStories['topStories'].length;j++){  
  //       if(storieIndex !== index)
  //         newStoriesOptions['topStories'].push(mainStories['topStories'][j])
  //       index++;
  //     }   
  //   }
  //   if(newStoriesOptions['mainStorie'])
  //     mainStories['mainStorie'] = newStoriesOptions['mainStorie'];
  //   mainStories['mainStorieListUl'] = newStoriesOptions['mainStorieListUl'];
  //   mainStories['topStories'] = newStoriesOptions['topStories']; 
  //   setAdminModeRefresh(true)
  // }  
  useEffect(()=>{ 
    const preLoadData = async () =>{
      await store.getAdvertisements();
      await store.getStoriesList();
      // store.stories.mainStorie = store.stories.mainStorie as any;
      // store.stories.mainStorieListUl = store.stories.mainStorie as any;
      // store.stories.topStories = store.stories.topStories as any;
      setIsReady(!isReady) 
    }
    preLoadData()
  },[ad,mainStories])
  return ( 
    <div className='App' style={themeMode.app}> 
    {!isReady ? <div>
      <div className="ui segment">
        <div className="ui active dimmer">
          <div className="ui text loader">Loading</div>
        </div>
        <p></p>
      </div>
    </div> : <>
      {params.admin && <AdminModal saveAd={saveAd} ad={ad} setOpenModal={setOpenModal} isOpenModal={isOpenModal} setAdminModeRefresh={setAdminModeRefresh} mainStories={store.stories}/>} 
      {params.admin && <div style={{position:'fixed',left:10,top:10,cursor:'pointer'}} onClick={()=>setOpenModal(true)}> <Icon name='adn' /></div>} 
      <GalleryContainer setGridAnime={setGridAnime} setColorMode={setColorMode} colorMode={colorMode}/>  
      {store.stories.mainStorie.item && <GridContainer setColorMode={setColorMode} colorMode={colorMode}/>  }
      <div className={`list-container-ad`} >
        <div className="list-view" style={themeMode.gridContainer.gridback}>
          <ListContainer isAdminModeRefresh={isAdminModeRefresh} setColorMode={setColorMode} colorMode={colorMode}/> 
          <div className={`list-ad`}>
            {ad.map((item: any)=>{
              // console.debug('item.isActive',item.isActive)
                if(!item.isActive)
                  return null;
                return (<img onClick={()=>window.open(item.url)} src={item.imgSrc} className={`baner`} alt={item.title}/>)
            })}
          </div>
        </div>
      </div>
      <div  id={`footer-grid`} style={{minHeight:500}} key={'4'}> 
          <div className={`footer-view`} style={themeMode.gridContainer.gridback}>
            <div className='gallery-header-left-link' style={{margin:20}}>
              <GiSlashedShield style={{fontSize:20,marginRight:5 }}/>
              <h3>insider journals</h3> 
            </div>
            <h2>Sign Up Now For A Free</h2>
            <h3>Consultation</h3>
            <div className='form-footer'>
              <div className='grid-footer-form'>
                <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='First Name'>
                  <Icon name='user' />
                  <input />
                </Input>
                <Input className={`footer-icon-form grid-footer-item`} iconPosition='left' placeholder='Last Name'>
                  <Icon name='user' />
                  <input />
                </Input> 
              </div>
                <Input className={`footer-icon-form`} iconPosition='left' placeholder='Email'>
                  <Icon name='mail' />
                  <input />
                </Input> 
                  <Dropdown className={`footer-icon-form`}/>  
                <Input className={`footer-icon-form`} iconPosition='left' placeholder='Phone'>
                  <Icon name='mobile' />
                  <input />
                </Input> 
            </div>
            <p style={{textAlign:'center'}}>By clicking Contact Me you verify that you are over 18, <br/>and agree to the terms and conditions</p>
            <Button animated='vertical' className={`footer-btn`}>
              <Button.Content hidden>
                Send
              </Button.Content>
              <Button.Content visible>Contact Me</Button.Content>
            </Button>
          </div> 
        </div> 
      <div className='footer' style={themeMode.gridContainer.gridback}>  
          <div className='gallery-header-left'>  
              <p style={themeMode.gridContainer.footerText}>developered by Avishay Tal</p> 
          </div>
          <div className='gallery-header-right'>
            <a href='https://www.twitter.com' className='gallery-footer-link'><FiTwitter style={themeMode.gridContainer.gridIcons}/></a>
            <a href='https://www.facebook.com' className='gallery-footer-link'><FiFacebook style={themeMode.gridContainer.gridIcons}/></a>
            <a href='https://www.instagram.com' className='gallery-footer-link'><FiInstagram style={themeMode.gridContainer.gridIcons}/></a> 
          </div> 
      </div>   
    </>} 
    </div> 
  );
  
}  

const AnimatedSwitch = withRouter(({ location, setDarkState, darkState }: any) => {  
  return (<TransitionGroup>
            <CSSTransition key={location.key} classNames={`slide`} timeout={3000}>
              <Switch location={location}>
                <Route path={`/`} component={()=><MainScreen/>} exact />   
                <Route path={`/admin`} component={()=><MainScreen admin/>} exact />
                <Route path={[`/News`,`/About`,`/Contact`]} component={(props: any)=><InfoScreen props={props}/>} />   
              </Switch>
            </CSSTransition>
          </TransitionGroup>)
});

function App() {   
  return (
    <StoreProvider> 
      <Helmet>
      <title>{ TITLE }</title>
      </Helmet>
      <ThemeProvider theme={theme}> 
        <Scrollbars id={'scroll'} style={{width:'100vw',height:'100vh',background: '#111111'}}>
            <BrowserRouter>
              <AnimatedSwitch/> 
            </BrowserRouter> 
          </Scrollbars>  
      </ThemeProvider> 
    </StoreProvider>
  );
}

export default App; 