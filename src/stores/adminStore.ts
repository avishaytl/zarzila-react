// export type TFriend = {
//     name: string
//     isFavorite: boolean
//     isSingle: boolean
//   }
  
  export function createStore() { 

    return {
      stories: {
        mainStorie: {},
        mainStorieListUl: [{}],
        topStories: [{}]
      } as any,
      async verifyLogin(setVerify: any, user: string, pass: string){
        try{ 
            let res = await fetch('http://localhost:4000/verifyLogin?user=' + user + '&pass=' + pass)
            if(res){ 
                let json = await res.json();
                await setVerify(json.response)
                console.debug('verifyLogin',json)
            }
        }catch(e){
            await setVerify(false)
            console.debug('verifyLogin',e)
        }
      },
      async getAdvertisements(){
        try{ 
            let res = await fetch('http://localhost:4000/getAdvertisements')
            if(res){ 
                let json = await res.json(); 
                console.debug('getAdvertisements',json)
            }
        }catch(e){
            console.debug('getAdvertisements',e)
        }
      },
      async getStoriesList(){
        try{ 
            let res = await fetch('http://localhost:4000/getStoriesList')
            let stories: any = {
                mainStorie: {},
                mainStorieListUl: [],
                topStories: []
              };
            let sqltables = [];
            if(res){ 
                let json = await res.json(); 
                if(json.data){
                    let tables = json.data;
                        for(let i = 0;i < tables.length;i++){ 
                            let res = await fetch(`http://localhost:4000/getStorie?tableName=${tables[i].table_name}`)
                            if(res){ 
                                let json = await res.json(); 
                                if(json.data){
                                    let table = json.data; 
                                    sqltables.push(table) 
                                }
                                // console.debug('getStorie',json.data)
                            }
                        } 
                }
                // console.debug('sqltables',sqltables.length) 
                if(sqltables.length)
                    sqltables.sort((e1: any,e2: any):any=>{
                        return e1[0][0].indexStorie - e2[0][0].indexStorie
                    }); 
                for(let i = 0;i < sqltables.length;i++){
                    let mainTitle= '',
                    subTitle= '',
                    mainText= '',
                    subText= '',
                    category= '',
                    createdAt= '',
                    readTime= '',
                    img= '',
                    title= '',
                    tableName= '',
                    index= '';
                    console.debug('sqltables[i].length',sqltables[i][0].length)
                    for(let j = 0;j < sqltables[i][0].length;j++){
                        mainTitle += sqltables[i][0][j].mainTitle;
                        subTitle += sqltables[i][0][j].subTitle;
                        mainText += sqltables[i][0][j].mainText;
                        subText += sqltables[i][0][j].subText;
                        category += sqltables[i][0][j].category;
                        createdAt += sqltables[i][0][j].createdAt;
                        readTime += sqltables[i][0][j].readTime;
                        img += sqltables[i][0][j].img;
                        title += sqltables[i][0][j].title;
                        index += sqltables[i][0][j].indexStorie;
                        tableName += sqltables[i][0][j].tableName;
                    } 
                    if(i + 1 === 1)
                        stories.mainStorie = { key: i + 1, item:{
                            mainTitle: mainTitle.replaceAll('_g_','"'),
                            subTitle: subTitle.replaceAll('_g_','"'),
                            mainText: mainText.replaceAll('_g_','"'),
                            subText: subText.replaceAll('_g_','"'),
                            category: category,
                            createdAt: createdAt,
                            readTime: readTime,
                            img: img.replaceAll("'",'"').replaceAll('_d_',':').replaceAll(`_b_`,"\\"),
                            title: title,
                            index: index,
                            tableName: tableName,
                        }}
                    else if(i + 1 >= 2 && i + 1 <= 5)
                        stories.mainStorieListUl.push({ key: i + 1, item:{
                                    mainTitle: mainTitle.replaceAll('_g_','"'),
                                    subTitle: subTitle.replaceAll('_g_','"'),
                                    mainText: mainText.replaceAll('_g_','"'),
                                    subText: subText.replaceAll('_g_','"'),
                                    category: category,
                                    createdAt: createdAt,
                                    readTime: readTime,
                                    img: img.replaceAll("'",'"').replaceAll('_d_',':').replaceAll(`_b_`,"\\"),
                                    title: title,
                                    index: index,
                                    tableName: tableName,
                        }})
                    else stories.topStories.push({ key: ((-1 * i - 4 + 8)), item:{
                                mainTitle: mainTitle.replaceAll('_g_','"'),
                                subTitle: subTitle.replaceAll('_g_','"'),
                                mainText: mainText.replaceAll('_g_','"'),
                                subText: subText.replaceAll('_g_','"'),
                                category: category,
                                createdAt: createdAt,
                                readTime: readTime,
                                img: img.replaceAll("'",'"').replaceAll('_d_',':').replaceAll(`_b_`,"\\"),
                                title: title,
                                index: index,
                                tableName: tableName,
                            }}) 
                    
                }
                console.debug('stories',stories) 
                this.stories = stories;
                // return stories;
            }
        }catch(e){ 
            console.debug('getStoriesList',e)
        }
      },
      async getStorie(tableName: string){
        try{ 
            let res = await fetch(`http://localhost:4000/getStorie?tableName=${tableName}`)
            if(res){ 
                let json = await res.json(); 
                console.debug('getStorie',json)
            }
        }catch(e){ 
            throw e;
        }
      },
      async createStorie(params: any){
        try{        
            const body = `{"Storie_Data":[{"tableName":"${params.tableName}"}]}`; 
            let res = await fetch('http://localhost:4000/createStorie',{
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/json', 
                  }),
                body: body,
            })
            if(res){ 
                let json = await res.json(); 
                console.debug('createStorie',json)
                if(json.code !== 200){
                    alert(json.msg)
                    // if(json.errCode === 'c1$23s4-create table'){ 
                        const body1 = `{"Storie_Data":[{"tableName":"${'newStorie'+ (1+parseInt(params.tableName[params.tableName.length - 1]))}"}]}`; 
                        let res = await fetch('http://localhost:4000/createStorie',{
                            method: 'POST',
                            headers: new Headers({
                                'Content-Type': 'application/json', 
                            }),
                            body: body1,
                        }) 
                        if(res){ 
                            let json = await res.json(); 
                            if(json.code !== 200){
                                alert(json.msg) 
                            }else{
                                alert(json.response) 
                                const body = `{"Storie_Data":[{"mainTitle":"${params.mainTitle}","subTitle":"${params.subTitle}","mainText":"${params.mainText}","subText":"${params.subText}",
                                    "category":"${params.category}","createdAt":"${params.createdAt}","readTime":"${params.readTime}","img":"${params.img}","tableName":"${'newStorie'+ (1+parseInt(params.tableName[params.tableName.length - 1]))}","index":"${(1+parseInt(params.index))}","title":"${'new '+ (1+parseInt(params.title[params.title.length - 1]))}"}]}`; 
                                let res = await fetch('http://localhost:4000/insertInto',{
                                    method: 'POST',
                                    headers: new Headers({
                                        'Content-Type': 'application/json', 
                                    }),
                                    body: body,
                                })
                                if(res){
                                    let json = await res.json(); 
                                    if(json.code !== 200){
                                        alert(json.msg) 
                                    }else{
                                        alert(json.response) 
                                    } 
                                } 
                            }
                        // }
                    }
                }else{
                    alert(json.response) 
                    const body = `{"Storie_Data":[{"mainTitle":"${params.mainTitle}","subTitle":"${params.subTitle}","mainText":"${params.mainText}","subText":"${params.subText}",
                        "category":"${params.category}","createdAt":"${params.createdAt}","readTime":"${params.readTime}","img":"${params.img}","tableName":"${params.tableName}","index":"${params.index}","title":"${params.title}"}]}`; 
                    let res = await fetch('http://localhost:4000/insertInto',{
                        method: 'POST',
                        headers: new Headers({
                            'Content-Type': 'application/json', 
                        }),
                        body: body,
                    })
                    if(res){
                        let json = await res.json(); 
                        if(json.code !== 200){
                            alert(json.msg) 
                        }else{
                            alert(json.response) 
                        } 
                    } 
                }
            }
        }catch(e){ 
            console.debug('createStorie',e)
        }
      },
      async updateStorie(params: any){
        try{        
            let mainTitle:any = {};
            let subTitle:any = {};
            let mainText:any = {};
            let subText:any = {};
            for(let i = 0;i < params.mainTitle.length || i < params.subTitle.length || i < params.mainText.length || i < params.subText.length;i++){
                if(i < params.mainTitle.length && params.mainTitle[i])
                    mainTitle[`${i}`] = params.mainTitle[i];
                if(i < params.subTitle.length && params.subTitle[i])
                    subTitle[`${i}`] = params.subTitle[i];
                if(i < params.mainText.length && params.mainText[i])
                    mainText[`${i}`] = params.mainText[i];
                if(i < params.subText.length && params.subText[i])
                    subText[`${i}`] = params.subText[i];
            } 
            const body = `{"Storie_Data":[{"mainTitle":"${JSON.stringify(mainTitle).replaceAll('"',`_jg_`)}","subTitle":"${JSON.stringify(subTitle).replaceAll('"',`_jg_`)}","mainText":"${JSON.stringify(mainText).replaceAll('"',`_jg_`)}","subText":"${JSON.stringify(subText).replaceAll('"',`_jg_`)}",
                        "category":"${params.category}","createdAt":"${params.createdAt}","readTime":"${params.readTime}","img":"${params.img}","tableName":"${params.tableName}","index":"${params.index}","title":"${params.title}"}]}`;
            console.debug('updateStorie',body) 
            let res = await fetch('http://localhost:4000/updateStorie',{
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/json', 
                  }),
                body: body,
            })
            if(res){ 
                let json = await res.json(); 
                console.debug('updateStorie',json)
                if(json.code !== 200){
                    // if('u1$23s4-update into' === json.errCode){
                        alert(json.msg + '\nWe Create For You!')
                    //     const body = `{"Storie_Data":[{"mainTitle":"${params.mainTitle}","subTitle":"${params.subTitle}","mainText":"${params.mainText}","subText":"${params.subText}",
                    //                 "category":"${params.category}","createdAt":"${params.createdAt}","readTime":"${params.readTime}","img":"${params.img}","title":"${params.title}"}]}`;
                    //     let res = await fetch('http://localhost:4000/createStorie',{
                    //         method: 'POST',
                    //         headers: new Headers({
                    //             'Content-Type': 'application/json', 
                    //           }),
                    //         body: body,
                    //     })
                    //     if(res){ 
                    //         let json = await res.json(); 
                    //         console.debug('updateStorie u1$23s4-update ',json)
                    //         if(json.code !== 200){
                    //             alert(json.msg)
                    //         }else{
                    //             alert(json.response) 
                    //         }
                    //     }
                    // }
                    console.debug('json.errCode',json.errCode)
                    // if(json.msg.indexOf(''))
                }else{
                    alert(json.response) 
                }
            }
        }catch(e){ 
            console.debug('updateStorie',e)
        }
      },
    //   makeFriend(name: string, isFavorite = false, isSingle = false) {
    //     const oldFriend = this.friends.find(friend => friend.name === name)
    //     if (oldFriend) {
    //       oldFriend.isFavorite = isFavorite
    //       oldFriend.isSingle = isSingle
    //     } else {
    //       this.friends.push({ name, isFavorite, isSingle })
    //     }
    //   },
    //   get singleFriends() {
    //     return this.friends.filter(friend => friend.isSingle)
    //   },
    }
  }
  
  export type TStore = ReturnType<typeof createStore>